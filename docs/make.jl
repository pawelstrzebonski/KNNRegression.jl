using Documenter
import KNNRegression

makedocs(
    sitename = "KNNRegression.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "KNNRegression.jl"),
    pages = [
        "Home" => "index.md",
        "Example Usage" =>"example.md",
        "src/" => ["KNNRegression.jl" => "KNNRegression.md"],
        "test/" => ["KNNRegression.jl" => "KNNRegression_test.md"],
    ],
)
