# KNNRegression.jl Documentation

`KNNRegression` is a package for k-nearest neighbors based regression.
This is a relatively simple algorithm to understand, and it allows for
a simple way of (some sort of) interpolation when the known points are
not on any sort of grid (something lacking in many Julia interpolation
packages).

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/KNNRegression.jl
```

## Features

* Given a training set, returns a function for regressing points onto said set

## TODOs

* More examples and tests
