# Example Usage

The original reason this package was made was the seeming lack of Julia packages
that would allow for interpolation when the given points are not on a
grid, making interpolation of polar coordinate grid data onto a Cartesian
grid difficult. As such, we'll use that as an example. We define a set
of points on a polar grid:

```@example ff
theta=0:0.1:(2*pi)
r=0:0.1:2
theta, r=vec(theta*ones(length(r))'), vec(ones(length(theta))*r')
z=@. exp(-r^2)*cos(2*theta)
nothing # hide
```

Now we convert the polar coordinates to Cartesian and create a k-NN regression function:

```@example ff
import KNNRegression
x, y=@. r*cos(theta), @. r*sin(theta)
points=[x y]'
f=KNNRegression.knnregressor(points, z)
nothing # hide
```

Now that we have the regressor function `f`, we can interpolate a
set of Cartesian grid coordinates and use them to plot the interpolated
results using the `Plots` package:

```@example ff
import Plots: heatmap
import Plots: savefig # hide
xrng=-2:0.01:2
yrng=-2:0.01:2
x, y=vec(xrng*ones(length(yrng))'), vec(ones(length(xrng))*yrng')
points=[x y]'
z=reshape(f(points), length(xrng), length(yrng))
heatmap(xrng, yrng, z)
savefig("interpolated.png"); nothing # hide
```

![Interpolation onto a Cartesian grid](interpolated.png)
