# KNNRegression.jl

Implement inverse-distance weighted k-NN regression. Uses the
[`NearestNeighbors`](https://github.com/KristofferC/NearestNeighbors.jl)
package for k-NN implementation, and can have the "tree"
back-end chosen using the `met` argument (other tree implementations
may be more efficient than the default we've chosen).

```@autodocs
Modules = [KNNRegression]
Pages   = ["KNNRegression.jl"]
```
