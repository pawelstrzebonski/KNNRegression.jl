xs=vec((0:10)*ones(11)')
ys=vec(ones(11)*(0:10)')
zs=@. xs+ys
points=[xs ys]'

@test (KNNRegression.knnregressor(points, zs); true)

f=KNNRegression.knnregressor(points, zs)

@test f(points)==zs
@test maximum(abs.(f(points.+1e-4).-zs))<1e-3
