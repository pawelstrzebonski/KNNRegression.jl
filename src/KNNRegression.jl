module KNNRegression

import NearestNeighbors

"""
    knnaverage(vals, dists)

Inverse-distance weighted regression.
"""
function knnaverage(vals, dists)
	# Handle the case in which the regressed point is in the training set
	if any(iszero.(dists))
		vals[findfirst(iszero, dists)]
	else
		weights=1 ./dists
		weights./=sum(weights)
		sum(vals.*weights)
	end
end

"""
    knnregressor(points, values; k=3)->regressor(points)

Create a function that will regress (effectively interpolate) an array
of points onto
the training dataset `points` and `values`.
"""
function knnregressor(points::AbstractMatrix, values::AbstractVector; k::Integer=3, met=:KDTree)
	@assert size(points, 2)==length(values)
	tree=NearestNeighbors.KDTree
	if met==:BruteTree
		tree=NearestNeighbors.BruteTree
	elseif met==:BallTree
		tree=NearestNeighbors.BallTree
	elseif met==:KDTree
		tree=NearestNeighbors.KDTree
	else
		@error "Unknown tree type"
	end
    knntree=tree(points)
    function regressor(pts::AbstractMatrix)
		@assert size(pts, 1)==size(points, 1)
		idxs, dists=NearestNeighbors.knn(knntree, pts, k)
		[knnaverage(values[idxs[i]], dists[i]) for i=1:length(idxs)]
	end
	regressor
end

export knnregressor

end
