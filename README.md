# KNNRegression

## About

`KNNRegression.jl` is a package for using k-nearest neighbors for
regression.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/KNNRegression.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for KNNRegression.jl](https://pawelstrzebonski.gitlab.io/KNNRegression.jl/).
